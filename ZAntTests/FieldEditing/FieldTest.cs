﻿
namespace ZAntTests
{
    using NUnit.Framework;
    using ZAnt.FieldEditing;

    /// <summary>
    /// This is a test class for FieldTest and is intended
    /// to contain all FieldTest Unit Tests
    /// </summary>
    [TestFixture]
    public class FieldTest
    {

        /// <summary>
        /// A test for setting and getting object
        /// </summary>
        [Test]
        public void SetGetObjectTest()
        {
            Field target = new Field(5, 5, false);
            FieldObjectTypes obj = FieldObjectTypes.Stone;
            target.SetObject(obj, 2, 3);
            Assert.AreEqual(target.GetCellObject(2, 3), obj);
        }

        /// <summary>
        /// A test for deleting object
        /// </summary>
        [Test]
        public void DeleteObjectTest()
        {
            Field target = new Field(5, 5, false);
            target.SetObject(FieldObjectTypes.Stone, 2, 3);
            target.DelObject(2, 3);
            Assert.AreEqual(target.GetCellObject(2, 3), FieldObjectTypes.FreeCell);
        }

        /// <summary>
        /// A test for moving object
        /// </summary>
        [Test]
        public void MoveObjectTest()
        {
            Field target = new Field(5, 5, false);
            FieldObjectTypes obj = FieldObjectTypes.Stone;
            target.SetObject(obj, 2, 3);
            target.MoveObject(2, 3, 3, 4);
            Assert.AreEqual(target.GetCellObject(2, 3), FieldObjectTypes.FreeCell);
            Assert.AreEqual(target.GetCellObject(3, 4), obj);
        }

    }
}
