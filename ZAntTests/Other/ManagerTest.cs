﻿
namespace ZAntTests
{
    using NUnit.Framework;
    using ZAnt.Other;

    /// <summary>
    /// This is a test class for ManagerTest and is intended
    /// to contain all ManagerTest Unit Tests
    /// </summary>
    [TestFixture]
    public class ManagerTest
    {

        /// <summary>
        /// A test for CreateField
        /// </summary>
        [Test]
        public void CreateFieldTest()
        {
            Manager target = Manager.GetInstance();
            target.CreateField(5, 5, false);
            Assert.NotNull(target.PlayField);
        }

    }
}
