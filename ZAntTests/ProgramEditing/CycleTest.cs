﻿
namespace ZAntTests
{
    using System.Collections.Generic;
    using NUnit.Framework;
    using ZAnt.ProgramEditing;

    /// <summary>
    /// This is a test class for CycleTest and is intended
    /// to contain all CycleTest Unit Tests
    /// </summary>
    [TestFixture]
    public class CycleTest
    {

        /// <summary>
        /// A test for RepeatCount set
        /// </summary>
        [Test]
        public void SetRepeatCountTest()
        {
            Cycle target = new Cycle(5, new List<AbstractCommand>());
            target.RepeatCount = 0;
            Assert.AreEqual(1, target.RepeatCount);
            target.RepeatCount = 1000;
            Assert.AreEqual(255, target.RepeatCount);
            target.RepeatCount = 128;
            Assert.AreEqual(128, target.RepeatCount);
        }

    }
}
