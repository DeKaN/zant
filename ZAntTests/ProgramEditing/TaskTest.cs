﻿
namespace ZAntTests
{
    using NUnit.Framework;
    using ZAnt.ProgramEditing;

    /// <summary>
    /// This is a test class for TaskTest and is intended
    /// to contain all TaskTest Unit Tests
    /// </summary>
    [TestFixture]
    public class TaskTest
    {

        /// <summary>
        /// A test for InsertCommand
        /// </summary>
        [Test]
        public void InsertCommandTest()
        {
            Task target = new Task();
            target.AddCommand(new Command());
            AbstractCommand command = new Command();
            command.CommandType = CommandType.Push;
            target.InsertCommand(0, command);
            Assert.AreEqual(command, target.GetCommands()[0]);
        }

        /// <summary>
        /// A test for GetCommands
        /// </summary>
        [Test]
        public void GetCommandsTest()
        {
            Task target = new Task();
            target.AddCommand(new Command());
            Assert.AreEqual(1, target.GetCommands().Count);
        }

        /// <summary>
        /// A test for DeleteCommand
        /// </summary>
        [Test]
        public void DeleteCommandTest()
        {
            Task target = new Task();
            AbstractCommand command = new Command();
            target.AddCommand(command);
            target.DeleteCommand(0);
            Assert.False(target.GetCommands().Contains(command));
        }

        /// <summary>
        /// A test for AddCommand
        /// </summary>
        [Test]
        public void AddCommandTest()
        {
            Task target = new Task();
            AbstractCommand command = new Command();
            target.AddCommand(command);
            Assert.True(target.GetCommands().Contains(command));
        }

    }
}
