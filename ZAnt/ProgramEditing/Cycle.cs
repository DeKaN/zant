﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Cycle.cs" company="">
//   
// </copyright>
// <summary>
//   Класс цикла.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ZAnt.ProgramEditing
{
    using System.Collections.Generic;

    using ZAnt.Other;

    /// <summary>
    /// Класс цикла.
    /// </summary>
    internal class Cycle : AbstractCommand
    {
        #region Constants and Fields

        /// <summary>
        /// The count.
        /// </summary>
        private int count;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="Cycle"/> .
        /// </summary>
        /// <param name="count">
        /// Количество итераций. 
        /// </param>
        /// <param name="commands">
        /// Список команд. 
        /// </param>
        public Cycle(int count, IList<AbstractCommand> commands)
        {
            this.CommandType = CommandType.Cycle;
            this.RepeatCount = CorrectRepeatCount(count);
            this.Commands = commands;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Получает или задает список команд.
        /// </summary>
        internal IList<AbstractCommand> Commands { get; set; }

        /// <summary>
        /// Получает или задает количество повторов цикла.
        /// </summary>
        internal int RepeatCount
        {
            get
            {
                return this.count;
            }

            set
            {
                this.count = CorrectRepeatCount(value);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Скорректировать значение количества повторов.
        /// </summary>
        /// <param name="value">
        /// Текущее количество.
        /// </param>
        /// <returns>
        /// Скорректированное количество, строго лежащее в допустимом интервале.
        /// </returns>
        private static int CorrectRepeatCount(int value)
        {
            if (value < 1)
            {
                return 1;
            }

            int max = Config.GetInstance().MaxCycleCounter;
            return value > max ? max : value;
        }

        #endregion
    }
}