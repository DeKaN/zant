﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AbstractCommand.cs" company="">
//   
// </copyright>
// <summary>
//   Абстрактный класс-родитель команд и типы команд.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ZAnt.ProgramEditing
{
    /// <summary>
    /// Тип команды.
    /// </summary>
    internal enum CommandType
    {
        /// <summary>
        ///   Переместиться.
        /// </summary>
        Move, 

        /// <summary>
        ///   Толкнуть.
        /// </summary>
        Push, 

        /// <summary>
        ///   Прыгнуть.
        /// </summary>
        Jump, 

        /// <summary>
        ///   Цикл.
        /// </summary>
        Cycle
    }

    /// <summary>
    /// Абстрактный класс-родитель команд.
    /// </summary>
    internal abstract class AbstractCommand
    {
        #region Public Properties

        /// <summary>
        ///   Получает или задает тип команды.
        /// </summary>
        internal CommandType CommandType { get; set; }

        #endregion
    }
}