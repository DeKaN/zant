// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Command.cs" company="">
//   
// </copyright>
// <summary>
//   ����� ������� � ����������� �������� �������.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ZAnt.ProgramEditing
{
    /// <summary>
    /// ����������� �������� �������.
    /// </summary>
    internal enum Direction
    {
        /// <summary>
        ///  �����.
        /// </summary>
        Left = 3, 

        /// <summary>
        ///  ������.
        /// </summary>
        Right = 1, 

        /// <summary>
        ///  �����.
        /// </summary>
        Up = 0, 

        /// <summary>
        ///  ����.
        /// </summary>
        Down = 2
    }

    /// <summary>
    /// ����� �������.
    /// </summary>
    internal class Command : AbstractCommand
    {
        #region Properties

        /// <summary>
        ///   �������� ��� ������ ����������� �������� �������.
        /// </summary>
        internal Direction Direction { get; set; }

        #endregion
    }
}