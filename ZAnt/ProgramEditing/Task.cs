﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Task.cs" company="">
//   
// </copyright>
// <summary>
//   Класс программы управления.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ZAnt.ProgramEditing
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Security.Cryptography;

    using ZAnt.Other;

    /// <summary>
    /// Класс программы управления.
    /// </summary>
    internal class Task
    {
        #region Constants and Fields

        /// <summary>
        /// Список комманд в программе управления.
        /// </summary>
        private readonly List<AbstractCommand> commands;

        /// <summary>
        /// Ссылка для хранения объекта менеджера.
        /// </summary>
        private readonly Manager manager = Manager.GetInstance();

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="Task"/> .
        /// </summary>
        internal Task()
            : this(new List<AbstractCommand>())
        {
        }

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="Task"/>.
        /// </summary>
        /// <param name="cmds">
        /// The cmds.
        /// </param>
        private Task(List<AbstractCommand> cmds)
        {
            this.commands = cmds;
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Загрузить из файлового потока.
        /// </summary>
        /// <param name="fs">
        /// Файловый поток, из которого будет производиться загрузка. 
        /// </param>
        /// <returns>
        /// Возвращает объект программы управления.
        /// </returns>
        /// <exception cref="FileFormatException">
        /// Возникает при несоответствии формату
        /// </exception>
        public static Task Load(FileStream fs)
        {
            Debug.Assert(fs != null, "Поток не существует!");
            BinaryReader br = new BinaryReader(fs);
            if (!br.ReadBytes(Config.TaskSignature.Length).SequenceEqual(Config.TaskSignature))
            {
                throw new FileFormatException("Сигнатура повреждена!");
            }

            int dataLength = br.ReadInt32();
            byte[] buffer = br.ReadBytes(dataLength), hash = new SHA1Managed().ComputeHash(buffer);
            if ((!br.ReadBytes(hash.Length).SequenceEqual(hash)) || (br.PeekChar() != -1))
            {
                throw new FileFormatException("Файл поврежден!");
            }

            br.Close();
            Task t;
            using (MemoryStream ms = new MemoryStream(buffer))
            {
                using (BinaryReader reader = new BinaryReader(ms))
                {
                    t = new Task(ReadCommands(reader));
                }
            }

            return t;
        }

        /// <summary>
        /// Сохранить в файловый поток.
        /// </summary>
        /// <param name="fs">
        /// Файловый поток, в который будет производиться сохранение. 
        /// </param>
        public void Save(FileStream fs)
        {
            Debug.Assert(fs != null, "Поток не существует!");
            BinaryWriter bw = new BinaryWriter(fs);
            bw.Write(Config.TaskSignature);
            byte[] buffer;
            using (MemoryStream ms = new MemoryStream())
            {
                using (BinaryWriter writer = new BinaryWriter(ms))
                {
                    this.WriteCommands(writer, this.commands);
                }

                buffer = ms.ToArray();
            }

            bw.Write(buffer.Length);
            bw.Write(buffer);
            bw.Write(new SHA1Managed().ComputeHash(buffer));
            bw.Close();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Добавить команду в указанное место.
        /// </summary>
        /// <param name="index">
        /// Индекс места, на которое будет вставлена команда. 
        /// </param>
        /// <param name="command">
        /// Команда, которая будет добавлена. 
        /// </param>
        internal void InsertCommand(int index, AbstractCommand command)
        {
            Debug.Assert((index >= 0) && (index <= this.commands.Count), "Попытка вставить в несуществующую позицию!");
            this.commands.Insert(index, command);
            this.manager.OnTaskChanged(new EventArgs());
        }

        /// <summary>
        /// Добавить команду в конец программы управления.
        /// </summary>
        /// <param name="command">
        /// Команда, которая будет добавлена. 
        /// </param>
        internal void AddCommand(AbstractCommand command)
        {
            this.commands.Add(command);
            this.manager.OnTaskChanged(new EventArgs());
        }

        /// <summary>
        /// Удалить команду из программы управления.
        /// </summary>
        /// <param name="index">
        /// Индекс команды. 
        /// </param>
        internal void DeleteCommand(int index)
        {
            Debug.Assert((index >= 0) && (index < this.commands.Count), "Нет комманды с таким индексом!");
            this.commands.RemoveAt(index);
            this.manager.OnTaskChanged(new EventArgs());
        }

        /// <summary>
        /// Получить список команд.
        /// </summary>
        /// <returns>
        /// Возвращает список команд 
        /// </returns>
        internal IList<AbstractCommand> GetCommands()
        {
            return this.commands;
        }

        /// <summary>
        /// Прочитать список команд из бинарного формата.
        /// </summary>
        /// <param name="reader">
        /// Объект, обеспечивающий чтение. 
        /// </param>
        /// <returns>
        /// Список прочитанных команд. 
        /// </returns>
        private static List<AbstractCommand> ReadCommands(BinaryReader reader)
        {
            int count = reader.ReadInt32();
            List<AbstractCommand> cmds = new List<AbstractCommand>(count);
            for (int i = 0; i < count; i++)
            {
                CommandType type = (CommandType)reader.ReadByte();
                AbstractCommand cmd;
                if (type != CommandType.Cycle)
                {
                    cmd = new Command
                        {
                           CommandType = type,
                           Direction = (Direction)reader.ReadByte() 
                        };
                }
                else
                {
                    cmd = new Cycle(reader.ReadByte(), ReadCommands(reader));
                }

                cmds.Add(cmd);
            }

            return cmds;
        }

        /// <summary>
        /// Записать список команд в бинарного формат.
        /// </summary>
        /// <param name="writer">
        /// Объект, обеспечивающий запись. 
        /// </param>
        /// <param name="cmds">
        /// Список записанных команд. 
        /// </param>
        private void WriteCommands(BinaryWriter writer, IList<AbstractCommand> cmds)
        {
            writer.Write(cmds.Count);
            foreach (AbstractCommand abstractCommand in cmds)
            {
                writer.Write((byte)abstractCommand.CommandType);
                if (abstractCommand.CommandType != CommandType.Cycle)
                {
                    Command command = abstractCommand as Command;
                    Debug.Assert(command != null, "Ошибка преобразования команды");
                    writer.Write((byte)command.Direction);
                }
                else
                {
                    Cycle cycle = abstractCommand as Cycle;
                    Debug.Assert(cycle != null, "Ошибка преобразования цикла");
                    writer.Write((byte)cycle.RepeatCount);
                    this.WriteCommands(writer, cycle.Commands);
                }
            }
        }

        #endregion
    }
}