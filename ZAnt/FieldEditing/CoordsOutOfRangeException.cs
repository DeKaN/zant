﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CoordsOutOfRangeException.cs" company="">
//   
// </copyright>
// <summary>
//   Исключение, возникающее при попытке выйти за пределы поля.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ZAnt.FieldEditing
{
    using System;

    /// <summary>
    /// Исключение, возникающее при попытке выйти за пределы поля.
    /// </summary>
    public class CoordsOutOfRangeException : SystemException
    {
        #region Constructors and Destructors

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="CoordsOutOfRangeException"/>.
        /// </summary>
        public CoordsOutOfRangeException()
            : base("Попытка выхода за пределы поля")
        {
        }

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="CoordsOutOfRangeException"/> заданным сообщением об ошибке.
        /// </summary>
        /// <param name="message">
        /// Сообщение, описывающее ошибку. 
        /// </param>
        public CoordsOutOfRangeException(string message)
            : base(message)
        {
        }

        #endregion
    }
}