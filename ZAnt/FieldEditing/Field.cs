﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Field.cs" company="">
//   
// </copyright>
// <summary>
//   Типы объектов, которые могут присутствовать на поле
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ZAnt.FieldEditing
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Security.Cryptography;

    using ZAnt.Other;

    /// <summary>
    /// Типы объектов, которые могут присутствовать на поле
    /// </summary>
    public enum FieldObjectTypes
    {
        /// <summary>
        /// Свободная клетка
        /// </summary>
        FreeCell, 

        /// <summary>
        /// Занятая клетка
        /// </summary>
        BusyCell, 

        /// <summary>
        /// Камень
        /// </summary>
        Stone, 

        /// <summary>
        /// Лунка
        /// </summary>
        Hole
    }

    /// <summary>
    /// Класс игрового поля
    /// </summary>
    internal sealed class Field
    {
        #region Constants and Fields

        /// <summary>
        /// Массив объектов игрового поля
        /// </summary>
        private readonly FieldObjectTypes[,] cells;

        /// <summary>
        /// Ссылка для хранения объекта менеджера.
        /// </summary>
        private readonly Manager manager = Manager.GetInstance();

        /// <summary>
        /// The max busy cells.
        /// </summary>
        private readonly int maxBusyCells;

        /// <summary>
        /// Максимальное количество лунок
        /// </summary>
        private readonly int maxHoles;

        /// <summary>
        /// Максимальное количество камней
        /// </summary>
        private readonly int maxStones;

        /// <summary>
        /// The busy cells.
        /// </summary>
        private int busyCells;

        /// <summary>
        /// Текущее количество лунок
        /// </summary>
        private int holes;

        /// <summary>
        /// Текущее количество камней
        /// </summary>
        private int stones;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="Field"/> и автоматически заполняет объектами.
        /// </summary>
        /// <param name="width">
        /// Ширина поля. 
        /// </param>
        /// <param name="height">
        /// Высота поля. 
        /// </param>
        public Field(int width, int height)
            : this(width, height, true)
        {
        }

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="Field"/>
        /// </summary>
        /// <param name="width">
        /// Ширина поля. 
        /// </param>
        /// <param name="height">
        /// Высота поля. 
        /// </param>
        /// <param name="autoGenerate">
        /// Генерировать ли объекты на поле. 
        /// </param>
        public Field(int width, int height, bool autoGenerate)
        {
            this.cells = new FieldObjectTypes[width, height];
            this.stones = this.holes = this.busyCells = 0;
            this.maxStones = this.maxHoles = this.maxBusyCells = width * height / 3;
            if (autoGenerate)
            {
                this.Generate();
            }
        }

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="Field"/>.
        /// </summary>
        /// <param name="original">
        /// Поле, копия которого будет создана.
        /// </param>
        internal Field(Field original)
            : this(original.Width, original.Height, false)
        {
            this.busyCells = original.busyCells;
            this.holes = original.holes;
            this.stones = original.stones;
            this.maxBusyCells = original.maxBusyCells;
            this.maxHoles = original.maxHoles;
            this.maxStones = original.maxStones;
            this.AntInstance = new Ant(original.AntInstance.X, original.AntInstance.Y);
            Array.Copy(original.cells, this.cells, this.cells.Length);
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Получает или задает ссылку на объект муравья.
        /// </summary>
        public Ant AntInstance { get; set; }

        /// <summary>
        /// Получает высоту поля.
        /// </summary>
        public int Height
        {
            get
            {
                return this.cells.GetLength(1);
            }
        }

        /// <summary>
        /// Получает ширину поля.
        /// </summary>
        public int Width
        {
            get
            {
                return this.cells.GetLength(0);
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Загрузить из файлового потока.
        /// </summary>
        /// <param name="fs">
        /// Файловый поток, из которого будет производиться загрузка. 
        /// </param>
        /// <returns>
        /// Возвращает объект поля 
        /// </returns>
        /// <exception cref="FileFormatException">
        /// Возникает при несоответствии формату
        /// </exception>
        public static Field Load(FileStream fs)
        {
            Debug.Assert(fs != null, "Поток не существует!");
            BinaryReader br = new BinaryReader(fs);
            if (!br.ReadBytes(Config.FieldSignature.Length).SequenceEqual(Config.FieldSignature))
            {
                throw new FileFormatException("Сигнатура повреждена!");
            }

            int dataLength = br.ReadInt32();
            byte[] buffer = br.ReadBytes(dataLength), hash = new SHA1Managed().ComputeHash(buffer);
            if ((!br.ReadBytes(hash.Length).SequenceEqual(hash)) || (br.PeekChar() != -1))
            {
                throw new FileFormatException("Файл поврежден!");
            }

            br.Close();
            Field f;
            using (MemoryStream ms = new MemoryStream(buffer))
            {
                using (BinaryReader reader = new BinaryReader(ms))
                {
                    int width = reader.ReadByte(), height = reader.ReadByte();
                    f = new Field(width, height, false);
                    for (int i = 0; i < width; i++)
                    {
                        for (int j = 0; j < height; j++)
                        {
                            try
                            {
                                f.SetObjectImplementation((FieldObjectTypes)reader.ReadByte(), i, j);
                            }
                            catch (ObjectsCountOutOfRangeException exc)
                            {
                                Debug.Assert(false, exc.Message);
                            }
                        }
                    }

                    f.AntInstance = new Ant(reader.ReadByte(), reader.ReadByte());
                }
            }

            return f;
        }

        /// <summary>
        /// Получить объект находящийся в ячейке с заданными координатами.
        /// </summary>
        /// <param name="x">
        /// Координата ячейки по горизонтали. 
        /// </param>
        /// <param name="y">
        /// Координата ячейки по вертикали. 
        /// </param>
        /// <exception cref="CoordsOutOfRangeException">
        /// Выкидывает исключение, если координаты не принадлежат полю
        /// </exception>
        /// <returns>
        /// Объект, расположенный в данной ячейке 
        /// </returns>
        public FieldObjectTypes GetCellObject(int x, int y)
        {
            this.CheckCoords(x, y);

            return this.cells[x, y];
        }

        /// <summary>
        /// Сохранить в файловый поток.
        /// </summary>
        /// <param name="fs">
        /// Файловый поток, в который будет производиться сохранение. 
        /// </param>
        public void Save(FileStream fs)
        {
            Debug.Assert(fs != null, "Поток не существует!");
            BinaryWriter bw = new BinaryWriter(fs);
            bw.Write(Config.FieldSignature);
            byte[] buffer;
            using (MemoryStream ms = new MemoryStream())
            {
                using (BinaryWriter writer = new BinaryWriter(ms))
                {
                    writer.Write((byte)this.Width);
                    writer.Write((byte)this.Height);
                    for (int i = 0; i < this.Width; i++)
                    {
                        for (int j = 0; j < this.Height; j++)
                        {
                            writer.Write((byte)this.cells[i, j]);
                        }
                    }

                    writer.Write((byte)this.AntInstance.X);
                    writer.Write((byte)this.AntInstance.Y);
                }

                buffer = ms.ToArray();
            }

            bw.Write(buffer.Length);
            bw.Write(buffer);
            bw.Write(new SHA1Managed().ComputeHash(buffer));
            bw.Close();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Удалить объект с поля.
        /// </summary>
        /// <param name="x">
        /// Координата по горизонтали. 
        /// </param>
        /// <param name="y">
        /// Координата по вертикали. 
        /// </param>
        /// <exception cref="CoordsOutOfRangeException">
        /// Выкидывает исключение, если координаты не принадлежат полю
        /// </exception>
        /// <exception cref="ObjectsCountOutOfRangeException">
        /// Выкидывает исключение, если при удалении количество объектов станет меньше минимального
        /// </exception>
        internal void DelObject(int x, int y)
        {
            this.DelObjectImplementation(x, y);
            this.manager.OnFieldChanged(new EventArgs());
        }

        /// <summary>
        /// Переместить объект поля из одной ячейки в другую
        /// </summary>
        /// <param name="oldX">
        /// Исходная координата по горизонтали. 
        /// </param>
        /// <param name="oldY">
        /// Исходная координата по вертикали. 
        /// </param>
        /// <param name="newX">
        /// Новая координата по горизонтали. 
        /// </param>
        /// <param name="newY">
        /// Новая координата по вертикали. 
        /// </param>
        /// <exception cref="CoordsOutOfRangeException">
        /// Выкидывает исключение, если координаты не принадлежат полю
        /// </exception>
        internal void MoveObject(int oldX, int oldY, int newX, int newY)
        {
            this.CheckCoords(oldX, oldY);
            this.CheckCoords(newX, newY);

            FieldObjectTypes obj = this.GetCellObject(oldX, oldY);
            try
            {
                this.DelObjectImplementation(oldX, oldY);
                this.SetObjectImplementation(obj, newX, newY);
                this.manager.OnFieldChanged(new EventArgs());
            }
            catch (ObjectsCountOutOfRangeException exc)
            {
                Debug.Assert(false, exc.Message);
            }
        }

        /// <summary>
        /// Установить объект на поле.
        /// </summary>
        /// <param name="obj">
        /// Новый объект, который будет помещен. 
        /// </param>
        /// <param name="x">
        /// Координата по горизонтали. 
        /// </param>
        /// <param name="y">
        /// Координата по вертикали. 
        /// </param>
        /// <exception cref="CoordsOutOfRangeException">
        /// Выкидывает исключение, если координаты не принадлежат полю
        /// </exception>
        /// <exception cref="ObjectsCountOutOfRangeException">
        /// Выкидывает исключение, если при добавлении количество объектов станет больше максимального
        /// </exception>
        internal void SetObject(FieldObjectTypes obj, int x, int y)
        {
            this.SetObjectImplementation(obj, x, y);
            this.manager.OnFieldChanged(new EventArgs());
        }

        /// <summary>
        /// Проверить лежат ли координаты внутри поля.
        /// </summary>
        /// <param name="x">
        /// Координата по горизонтали. 
        /// </param>
        /// <param name="y">
        /// Координата по вертикали. 
        /// </param>
        /// <exception cref="CoordsOutOfRangeException">
        /// Выкидывает исключение, если координаты не принадлежат полю
        /// </exception>
        private void CheckCoords(int x, int y)
        {
            if ((x < 0) || (y < 0) || (x >= this.Width) || (y >= this.Height))
            {
                throw new CoordsOutOfRangeException();
            }
        }

        /// <summary>
        /// Реализация удаления, рекомендуется использовать <see cref="DelObject"/> .
        /// </summary>
        /// <param name="x">
        /// Координата по горизонтали. 
        /// </param>
        /// <param name="y">
        /// Координата по вертикали. 
        /// </param>
        /// <exception cref="CoordsOutOfRangeException">
        /// Выкидывает исключение, если координаты не принадлежат полю
        /// </exception>
        /// <exception cref="ObjectsCountOutOfRangeException">
        /// Выкидывает исключение, если при удалении количество объектов станет меньше минимального
        /// </exception>
        private void DelObjectImplementation(int x, int y)
        {
            this.CheckCoords(x, y);

            switch (this.cells[x, y])
            {
                case FieldObjectTypes.BusyCell:
                    if (this.busyCells == 0)
                    {
                        throw new ObjectsCountOutOfRangeException(
                            this.cells[x, y], "Занятых клеток уже не должно быть - что-то не так");
                    }

                    this.busyCells--;
                    break;
                case FieldObjectTypes.Stone:
                    if (this.stones == 0)
                    {
                        throw new ObjectsCountOutOfRangeException(
                            this.cells[x, y], "Камней уже не должно быть - что-то не так");
                    }

                    this.stones--;
                    break;
                case FieldObjectTypes.Hole:
                    if (this.holes == 0)
                    {
                        throw new ObjectsCountOutOfRangeException(
                            this.cells[x, y], "Лунок уже не должно быть - что-то не так");
                    }

                    this.holes--;
                    break;
            }

            this.cells[x, y] = FieldObjectTypes.FreeCell;
        }

        /// <summary>
        /// Заполнить поле объектами.
        /// </summary>
        private void Generate()
        {
            int needStones = this.maxStones - this.stones,
                needHoles = this.maxHoles - this.holes,
                needBusyCells = this.maxBusyCells - this.busyCells;
            for (int i = 0; i < needStones; i++)
            {
                int x, y;
                this.GetFreeCoords(out x, out y);
                this.SetObjectImplementation(FieldObjectTypes.Stone, x, y);
            }

            for (int i = 0; i < needHoles; i++)
            {
                int x, y;
                this.GetFreeCoords(out x, out y);
                this.SetObjectImplementation(FieldObjectTypes.Hole, x, y);
            }

            for (int i = 0; i < needBusyCells; i++)
            {
                int x, y;
                this.GetFreeCoords(out x, out y);
                this.SetObjectImplementation(FieldObjectTypes.BusyCell, x, y);
            }
        }

        /// <summary>
        /// Получить координаты свободной ячейки на поле.
        /// </summary>
        /// <param name="x">
        /// Координата по горизонтали. 
        /// </param>
        /// <param name="y">
        /// Координата по вертикали. 
        /// </param>
        private void GetFreeCoords(out int x, out int y)
        {
            Random rand = new Random();
            do
            {
                x = rand.Next(this.Width);
                y = rand.Next(this.Height);
            }
            while (this.GetCellObject(x, y) != FieldObjectTypes.FreeCell);
        }

        /// <summary>
        /// Реализация установки объекта на поле, рекомендуется использовать <see cref="SetObject"/> .
        /// </summary>
        /// <param name="obj">
        /// Новый объект, который будет помещен. 
        /// </param>
        /// <param name="x">
        /// Координата по горизонтали. 
        /// </param>
        /// <param name="y">
        /// Координата по вертикали. 
        /// </param>
        /// <exception cref="CoordsOutOfRangeException">
        /// Выкидывает исключение, если координаты не принадлежат полю
        /// </exception>
        /// <exception cref="ObjectsCountOutOfRangeException">
        /// Выкидывает исключение, если при добавлении количество объектов станет больше максимального
        /// </exception>
        private void SetObjectImplementation(FieldObjectTypes obj, int x, int y)
        {
            this.CheckCoords(x, y);

            if (this.cells[x, y] == obj)
            {
                return;
            }

            FieldObjectTypes oldObject = this.GetCellObject(x, y);
            this.DelObjectImplementation(x, y);

            switch (obj)
            {
                case FieldObjectTypes.BusyCell:
                    if (this.busyCells == this.maxBusyCells)
                    {
                        // Восстанавливаем удаленный объект
                        this.SetObjectImplementation(oldObject, x, y);
                        throw new ObjectsCountOutOfRangeException(
                            obj, "Попытка превысить максимальное количество занятых клеток");
                    }

                    this.busyCells++;
                    this.cells[x, y] = FieldObjectTypes.BusyCell;
                    break;
                case FieldObjectTypes.Stone:
                    if (this.stones == this.maxStones)
                    {
                        // Восстанавливаем удаленный объект
                        this.SetObjectImplementation(oldObject, x, y);
                        throw new ObjectsCountOutOfRangeException(
                            obj, "Попытка превысить максимальное количество камней");
                    }

                    this.stones++;
                    this.cells[x, y] = FieldObjectTypes.Stone;
                    break;
                case FieldObjectTypes.Hole:
                    if (this.holes == this.maxHoles)
                    {
                        // Восстанавливаем удаленный объект
                        this.SetObjectImplementation(oldObject, x, y);
                        throw new ObjectsCountOutOfRangeException(
                            obj, "Попытка превысить максимальное количество лунок");
                    }

                    this.holes++;
                    this.cells[x, y] = FieldObjectTypes.Hole;
                    break;
            }
        }

        #endregion
    }
}