﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Ant.cs" company="">
//   
// </copyright>
// <summary>
//   Класс муравья
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ZAnt.FieldEditing
{
    using System;

    using ZAnt.Other;
    using ZAnt.ProgramEditing;

    /// <summary>
    /// Класс муравья
    /// </summary>
    internal sealed class Ant
    {
        #region Constants and Fields

        /// <summary>
        /// Ссылка для хранения объекта менеджера.
        /// </summary>
        private readonly Manager manager = Manager.GetInstance();

        /// <summary>
        /// Текущее направление муравья.
        /// </summary>
        private Direction currentDirection = Direction.Up;

        /// <summary>
        /// Координата по горизонтали.
        /// </summary>
        private int x;

        /// <summary>
        /// Координата по вертикали.
        /// </summary>
        private int y;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="Ant"/> .
        /// </summary>
        /// <param name="x">
        /// Начальная координата по горизонтали. 
        /// </param>
        /// <param name="y">
        /// Начальная координата по вертикали. 
        /// </param>
        public Ant(int x, int y)
        {
            // dirty hack number 1
            //this.CheckCoords(x, y);

            this.x = x;
            this.y = y;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Получает или задает координату муравья по горизонтали.
        /// </summary>
        public int X
        {
            get
            {
                return this.x;
            }

            internal set
            {
                this.MoveTo(value, this.y);
            }
        }

        /// <summary>
        /// Получает или задает координату муравья по вертикали.
        /// </summary>
        public int Y
        {
            get
            {
                return this.y;
            }

            internal set
            {
                this.MoveTo(this.x, value);
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Получает или задает текущее направление муравья.
        /// </summary>
        internal Direction CurrentDirection
        {
            get
            {
                return this.currentDirection;
            }

            set
            {
                this.currentDirection = value;
                this.manager.OnFieldChanged(new EventArgs());
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Переместить муравья в заданные координаты поля.
        /// </summary>
        /// <param name="newX">
        /// Новая координата муравья по горизонтали. 
        /// </param>
        /// <param name="newY">
        /// Новая координата муравья по вертикали. 
        /// </param>
        /// <exception cref="CoordsOutOfRangeException">
        /// Выкидывает исключение, если координаты не принадлежат полю
        /// </exception>
        public void MoveTo(int newX, int newY)
        {
            this.CheckCoords(newX, newY);
            this.x = newX;
            this.y = newY;
            this.manager.OnFieldChanged(new EventArgs());
        }

        #endregion

        #region Methods

        /// <summary>
        /// Проверить лежат ли координаты внутри поля.
        /// </summary>
        /// <param name="newX">
        /// Координата по горизонтали. 
        /// </param>
        /// <param name="newY">
        /// Координата по вертикали. 
        /// </param>
        /// <exception cref="CoordsOutOfRangeException">
        /// Выкидывает исключение, если координаты не принадлежат полю
        /// </exception>
        private void CheckCoords(int newX, int newY)
        {
            if ((newX < 0) || (newY < 0) || (newX >= this.manager.PlayField.Width)
                || (newY >= this.manager.PlayField.Height))
            {
                throw new CoordsOutOfRangeException();
            }
        }

        #endregion
    }
}