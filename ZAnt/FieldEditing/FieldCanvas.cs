﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FieldCanvas.cs" company="">
//   
// </copyright>
// <summary>
//   Класс для отображения поля.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ZAnt.FieldEditing
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics.CodeAnalysis;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    using ZAnt.Other;

    /// <summary>
    /// Класс для отображения поля.
    /// </summary>
    public class FieldCanvas : Canvas
    {
        #region Constants and Fields

        /// <summary>
        /// Свойство, задающее фоновое изображение.
        /// </summary>
        public static readonly DependencyProperty CanvasImageSourceProperty =
            DependencyProperty.Register(
                "CanvasImageSource",
                typeof(ImageSource),
                typeof(FieldCanvas),
                new FrameworkPropertyMetadata(default(ImageSource)));

        /// <summary>
        /// Ссылка на экземпляр конфигурации.
        /// </summary>
        private readonly Config config = Config.GetInstance();

        /// <summary>
        /// Хранит изображения объектов поля.
        /// </summary>
        [SuppressMessage("StyleCop.CSharp.LayoutRules", "SA1500:CurlyBracketsForMultiLineStatementsMustNotShareLine",
            Justification = "StyleCop bug, this is not If")]
        private readonly Dictionary<int, ImageSource> fieldImages = new Dictionary<int, ImageSource>
            {
                { 23, new BitmapImage(new Uri("pack://application:,,,/images/feature25.png")) },
                { 24, new BitmapImage(new Uri("pack://application:,,,/images/bug25.png")) },
                { 26, new BitmapImage(new Uri("pack://application:,,,/images/busy25.png")) },
                { 27, new BitmapImage(new Uri("pack://application:,,,/images/stone25.png")) },
                { 28, new BitmapImage(new Uri("pack://application:,,,/images/hole25.png")) },
                { 48, new BitmapImage(new Uri("pack://application:,,,/images/feature50.png")) },
                { 49, new BitmapImage(new Uri("pack://application:,,,/images/bug50.png")) },
                { 51, new BitmapImage(new Uri("pack://application:,,,/images/busy50.png")) },
                { 52, new BitmapImage(new Uri("pack://application:,,,/images/stone50.png")) },
                { 53, new BitmapImage(new Uri("pack://application:,,,/images/hole50.png")) }
            };

        /// <summary>
        /// Кисть для отрисовки линий сетки.
        /// </summary>
        private readonly Pen gridPen = new Pen(Brushes.Black, 1);

        #endregion

        #region Public Properties

        /// <summary>
        /// Получает или задает фоновое изображение.
        /// </summary>
        public ImageSource CanvasImageSource
        {
            get
            {
                return (ImageSource)this.GetValue(CanvasImageSourceProperty);
            }

            set
            {
                this.SetValue(CanvasImageSourceProperty, value);
            }
        }

        /// <summary>
        /// Получает текущий размер ячейки в пикселях.
        /// </summary>
        public int CellSize { get; private set; }

        #endregion

        #region Methods

        /// <summary>
        /// Переопределение стандартного метода отрисовки.
        /// </summary>
        /// <param name="dc">
        /// Инструкции по рисованию конкретного элемента. Данный контекст предоставляется системе структуры. 
        /// </param>
        protected override void OnRender(DrawingContext dc)
        {
            dc.DrawImage(this.CanvasImageSource, new Rect(this.RenderSize));
            Field field = Manager.GetInstance().PlayField;
            int w = 0, h = 0;
            if (field != null)
            {
                w = field.Width;
                h = field.Height;
            }

            this.CellSize = ((w > this.config.MaxCells / 2) || (h > this.config.MaxCells / 2))
                                ? this.config.MinCellSize
                                : this.config.MaxCellSize;
            if (field != null)
            {
                // TODO: if task completed antIndex = -2
                int antIndex = -1;
                for (int i = 0; i < w; i++)
                {
                    for (int j = 0; j < h; j++)
                    {
                        FieldObjectTypes currCell = field.GetCellObject(i, j);
                        if (currCell != FieldObjectTypes.FreeCell)
                        {
                            this.DrawImageOnField(dc, this.fieldImages[this.CellSize + (int)currCell], i, j);
                        }
                    }
                }

                antIndex += this.CellSize;
                if (field.AntInstance != null)
                {
                    RotateTransform transform = new RotateTransform(90 * (int)field.AntInstance.CurrentDirection);
                    ImageSource antImg = new TransformedBitmap((BitmapSource)this.fieldImages[antIndex], transform);
                    this.DrawImageOnField(dc, antImg, field.AntInstance.X, field.AntInstance.Y);
                }
            }

            w = (w * this.CellSize) + 1;
            h = (h * this.CellSize) + 1;
            this.Width = w;
            this.Height = h;

            for (double x = 0; x < w; x += this.CellSize)
            {
                dc.DrawLine(this.gridPen, new Point(x, 0), new Point(x, h));
            }

            for (double y = 0; y < h; y += this.CellSize)
            {
                dc.DrawLine(this.gridPen, new Point(0, y), new Point(w, y));
            }

            base.OnRender(dc);
        }

        /// <summary>
        /// Нарисовать изображение объекта на поле.
        /// </summary>
        /// <param name="dc">
        /// Инструкции по рисованию конкретного элемента. Данный контекст предоставляется системе структуры. 
        /// </param>
        /// <param name="image">
        /// Изображение объекта, которое будет нарисовано. 
        /// </param>
        /// <param name="x">
        /// Координата на поле по горизонтали. 
        /// </param>
        /// <param name="y">
        /// Координата на поле по вертикали. 
        /// </param>
        private void DrawImageOnField(DrawingContext dc, ImageSource image, int x, int y)
        {
            Rect drawPosition = new Rect(x * this.CellSize, y * this.CellSize, this.CellSize, this.CellSize);
            dc.DrawImage(image, drawPosition);
        }

        #endregion
    }
}