﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ObjectsCountOutOfRangeException.cs" company="">
//   
// </copyright>
// <summary>
//   The objects count out of range exception.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ZAnt.FieldEditing
{
    using System;

    /// <summary>
    /// The objects count out of range exception.
    /// </summary>
    public class ObjectsCountOutOfRangeException : SystemException
    {
        #region Constructors and Destructors

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="ObjectsCountOutOfRangeException"/> .
        /// </summary>
        /// <param name="obj">
        /// Объект поля, количество которых вышло за допустимые пределы.
        /// </param>
        public ObjectsCountOutOfRangeException(FieldObjectTypes obj)
            : this(obj, "Количество объектов вышло за допустимые пределы!")
        {
        }

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="ObjectsCountOutOfRangeException"/> заданным сообщением об ошибке.
        /// </summary>
        /// <param name="obj">
        /// Объект поля, количество которых вышло за допустимые пределы.
        /// </param>
        /// <param name="message">
        /// Сообщение, описывающее ошибку. 
        /// </param>
        public ObjectsCountOutOfRangeException(FieldObjectTypes obj, string message)
            : base(message)
        {
            this.FieldObject = obj;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Получает объект, количество которых превысило максимальное.
        /// </summary>
        public FieldObjectTypes FieldObject { get; private set; }

        #endregion
    }
}