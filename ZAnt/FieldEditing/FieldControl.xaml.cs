﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FieldControl.xaml.cs" company="">
//   
// </copyright>
// <summary>
//   Логика взаимодействия для FieldControl.xaml
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ZAnt.FieldEditing
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Threading;

    using ZAnt.Other;

    /// <summary>
    /// Логика взаимодействия для FieldControl.xaml
    /// </summary>
    public partial class FieldControl : UserControl
    {
        #region Constants and Fields

        /// <summary>
        /// Ссылка для хранения объекта менеджера.
        /// </summary>
        private readonly Manager manager = Manager.GetInstance();

        /// <summary>
        /// Включен ли режим редактирования
        /// </summary>
        private bool editMode = true;

        /// <summary>
        /// Хранит выбранную в данный момент кнопку.
        /// </summary>
        private Button selectedBtn;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="FieldControl"/> .
        /// </summary>
        public FieldControl()
        {
            this.InitializeComponent();
            this.manager.FieldChanged += this.PlayFieldChanged;

            this.SelectButton(this.btnAnt);
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Получает или задает значение, показывающее, включен ли режим редактирования.
        /// </summary>
        public bool EditMode
        {
            get
            {
                return this.editMode;
            }

            set
            {
                this.editMode = value;

                this.btnAnt.Visibility =
                    this.btnBusycell.Visibility =
                    this.btnFreecell.Visibility =
                    this.btnHole.Visibility =
                    this.btnStone.Visibility = this.editMode ? Visibility.Visible : Visibility.Hidden;
                this.fieldImage.InvalidateVisual();
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Вызывается при щелчке по одной из кнопок объекта.
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения. 
        /// </param>
        private void ButtonsClick(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            if (button != null)
            {
                this.SelectButton(button);
            }
        }

        /// <summary>
        /// Устанавливает выделение кнопки выбранного типа объекта.
        /// </summary>
        /// <param name="button">
        /// Кнопка выбранного типа.
        /// </param>
        private void SelectButton(Button button)
        {
            if (this.selectedBtn != null)
            {
                this.selectedBtn.IsEnabled = true;
            }

            this.selectedBtn = button;
            if (this.selectedBtn != null)
            {
                this.selectedBtn.IsEnabled = false;
            }
        }

        /// <summary>
        /// Вызывается при отпускании левой кнопки мыши на игровом поле.
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения. 
        /// </param>
        private void FieldImageMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (this.manager.IsTaskRunning)
            {
                return;
            }

            if (this.selectedBtn == null)
            {
                return;
            }

            int id = int.Parse(this.selectedBtn.Uid);
            Point clickPoint = e.GetPosition(this.fieldImage);
            int x = (int)clickPoint.X / this.fieldImage.CellSize, y = (int)clickPoint.Y / this.fieldImage.CellSize;
            Field f = this.manager.PlayField;
            FieldObjectTypes obj;
            if (id == -1)
            {
                obj = f.GetCellObject(x, y);
                if ((obj == FieldObjectTypes.FreeCell) || (obj == FieldObjectTypes.BusyCell))
                {
                    f.AntInstance.MoveTo(x, y);
                }
                else
                {
                    MessageBox.Show("Муравья надо ставить на ячейку без камня/лунки!");
                }
            }
            else
            {
                obj = (FieldObjectTypes)id;
                if ((f.AntInstance.X == x) && (f.AntInstance.Y == y) && (obj != FieldObjectTypes.BusyCell)
                    && (obj != FieldObjectTypes.FreeCell))
                {
                    MessageBox.Show("Нельзя поставить камень/лунку на одну клетку с муравьем!");
                }
                else
                {
                    try
                    {
                        f.SetObject((FieldObjectTypes)id, x, y);
                    }
                    catch (ObjectsCountOutOfRangeException exc)
                    {
                        MessageBox.Show(exc.Message);
                    }
                }
            }
        }

        /// <summary>
        /// Вызывается, когда отображаемый объект <see cref="Field"/> сообщает об изменениях в структуре.
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения. 
        /// </param>
        private void PlayFieldChanged(object sender, EventArgs e)
        {
            this.Dispatcher.BeginInvoke(DispatcherPriority.Render, new Action(() => this.fieldImage.InvalidateVisual()));
        }

        #endregion
    }
}