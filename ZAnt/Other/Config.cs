﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Config.cs" company="">
//   
// </copyright>
// <summary>
//   Класс конфигурации.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ZAnt.Other
{
    /// <summary>
    /// Класс конфигурации.
    /// </summary>
    internal sealed class Config
    {
        #region Constants and Fields

        /// <summary>
        /// Сигнатура поля.
        /// </summary>
        internal static readonly byte[] FieldSignature = new byte[] { 0x12, 0x04, 0x18, 0 };

        /// <summary>
        /// Сигнатура программы управления.
        /// </summary>
        internal static readonly byte[] TaskSignature = new byte[] { 0x0F, 0x12, 0x18, 0 };

        /// <summary>
        ///   Ссылка для хранения объекта конфигурации.
        /// </summary>
        private static Config instance;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///   Предотвращает вызов конструктора по умолчанию для класса <see cref="Config"/> и задает стандартные параметры.
        /// </summary>
        private Config()
        {
            this.MinCellSize = 25;
            this.MaxCellSize = 50;
            this.MaxCycleCounter = 255;
            this.MaxCycleNesting = 5;
            this.MinCells = 5;
            this.MaxCells = 16;
        }

        #endregion

        #region Properties

        /// <summary>
        ///   Получает размер большой клетки в пикселях
        /// </summary>
        internal int MaxCellSize { get; private set; }

        /// <summary>
        ///   Получает максимальное количество клеток.
        /// </summary>
        internal int MaxCells { get; private set; }

        /// <summary>
        ///   Получает максимальное количество итераций цикла.
        /// </summary>
        internal int MaxCycleCounter { get; private set; }

        /// <summary>
        ///   Получает максимальное вложенных циклов.
        /// </summary>
        internal int MaxCycleNesting { get; private set; }

        /// <summary>
        ///   Получает размер маленькой клетки в пикселях
        /// </summary>
        internal int MinCellSize { get; private set; }

        /// <summary>
        ///   Получает минимальное количество клеток.
        /// </summary>
        internal int MinCells { get; private set; }

        #endregion

        #region Methods

        /// <summary>
        /// Возвращает объект конфигурации.
        /// </summary>
        /// <returns>
        /// Объект, содержащий конфигурацию. 
        /// </returns>
        internal static Config GetInstance()
        {
            return instance ?? (instance = new Config());
        }

        #endregion
    }
}