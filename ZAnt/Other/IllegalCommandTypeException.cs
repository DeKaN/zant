﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IllegalCommandTypeException.cs" company="">
//   
// </copyright>
// <summary>
//   Исключение, возникающее при появлении неизвестного типа команд.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ZAnt.Other
{
    using System;

    using ZAnt.ProgramEditing;

    /// <summary>
    /// Исключение, возникающее при появлении неизвестного типа команд.
    /// </summary>
    internal class IllegalCommandTypeException : Exception
    {
        #region Constructors and Destructors

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="IllegalCommandTypeException"/>.
        /// </summary>
        /// <param name="command">
        /// Команда, вызвавшая исключение.
        /// </param>
        public IllegalCommandTypeException(AbstractCommand command)
            : this("Неизвестный тип команды!", command)
        {
        }

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="IllegalCommandTypeException"/> заданным сообщением об ошибке.
        /// </summary>
        /// <param name="message">
        /// Сообщение, описывающее ошибку.
        /// </param>
        /// <param name="command">
        /// Команда, вызвавшая исключение.
        /// </param>
        public IllegalCommandTypeException(string message, AbstractCommand command)
            : base(message)
        {
            this.Command = command;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Получает команду, вызвавшую исключение.
        /// </summary>
        public AbstractCommand Command { get; private set; }

        #endregion
    }
}