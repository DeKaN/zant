﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ExtendedTickBar.cs" company="">
//   
// </copyright>
// <summary>
//   The extended tick bar.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ZAnt.Other
{
    using System.Globalization;
    using System.Windows;
    using System.Windows.Controls.Primitives;
    using System.Windows.Media;

    /// <summary>
    /// Расширенный элемент-основа для SlideBar.
    /// </summary>
    internal class ExtendedTickBar : TickBar
    {
        #region Methods

        /// <summary>
        /// Переопределение стандартного метода отрисовки.
        /// </summary>
        /// <param name="dc">
        /// Инструкции по рисованию конкретного элемента. Данный контекст предоставляется системе структуры. 
        /// </param>
        protected override void OnRender(DrawingContext dc)
        {
            int num = (int)((this.Maximum - this.Minimum) / this.TickFrequency);
            for (int i = 0; i <= num; i++)
            {
                string caption = (i * this.TickFrequency + this.Minimum).ToString(CultureInfo.InvariantCulture);
                if (caption.Length == 1)
                {
                    caption = " " + caption;
                }

                FormattedText formattedText = new FormattedText(
                    caption, 
                    CultureInfo.InvariantCulture, 
                    FlowDirection.LeftToRight, 
                    new Typeface("Verdana"), 
                    8, 
                    Brushes.Black);
                double x = i * (this.ActualWidth / num - caption.Length / 2.0);
                dc.DrawText(formattedText, new Point(x, 25));
            }
        }

        #endregion
    }
}