﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Manager.cs" company="">
//   
// </copyright>
// <summary>
//   Класс менеджера.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ZAnt.Other
{
    using System;
    using System.Diagnostics;

    using ZAnt.FieldEditing;
    using ZAnt.ProgramEditing;

    /// <summary>
    /// Класс менеджера.
    /// </summary>
    internal class Manager
    {
        #region Constants and Fields

        /// <summary>
        /// Ссылка для хранения объекта менеджера.
        /// </summary>
        private static Manager instance;

        /// <summary>
        /// Ссылка для хранения объекта поля.
        /// </summary>
        private Field field;

        /// <summary>
        /// Задержка между выполнением двух соседних команд.
        /// </summary>
        private int latency;

        /// <summary>
        /// Ссылка для хранения объекта программы управления.
        /// </summary>
        private Task task;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Предотвращает вызов конструктора по умолчанию для класса <see cref="Manager"/>.
        /// </summary>
        private Manager()
        {
            this.IsTaskRunning = false;
            this.Latency = 300;
        }

        #endregion

        #region Public Events

        /// <summary>
        /// Возникает при изменении объектов на поле.
        /// </summary>
        public event EventHandler FieldChanged;

        /// <summary>
        /// Возникает при изменении программы управления.
        /// </summary>
        public event EventHandler TaskChanged;

        #endregion

        #region Public Properties

        /// <summary>
        /// Получает или задает значение, показывающее, запущено ли выполнение программы управления.
        /// </summary>
        public bool IsTaskRunning { get; internal set; }

        #endregion

        #region Properties

        /// <summary>
        /// Получает или задает задержку между выполнением двух соседних команд.
        /// </summary>
        internal int Latency
        {
            get
            {
                return this.latency;
            }

            set
            {
                if (value < 100)
                {
                    this.latency = 100;
                }
                else if (value > 1000)
                {
                    this.latency = 1000;
                }
                else
                {
                    this.latency = value;
                }
            }
        }

        /// <summary>
        /// Получает или задает игровое поле.
        /// </summary>
        internal Field PlayField
        {
            get
            {
                return this.field;
            }

            set
            {
                Debug.Assert(!this.IsTaskRunning, "Попытка изменить поле во время работы программы управления!");
                this.field = value;
                this.OnFieldChanged(new EventArgs());
            }
        }

        /// <summary>
        /// Получает или задает ссылку на программу управления для выполнения.
        /// </summary>
        internal Task TaskInstance
        {
            get
            {
                return this.task;
            }

            set
            {
                Debug.Assert(!this.IsTaskRunning, "Попытка изменить поле во время работы программы управления!");
                this.task = value;
                this.OnTaskChanged(new EventArgs());
            }
        }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Создать поле с указанными параметрами.
        /// </summary>
        /// <param name="width">
        /// Ширина поля. 
        /// </param>
        /// <param name="height">
        /// Высота поля. 
        /// </param>
        /// <param name="autogenerate">
        /// Заполнить ли автоматически поле объектами. 
        /// </param>
        public void CreateField(int width, int height, bool autogenerate)
        {
            this.PlayField = new Field(width, height, autogenerate);
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    FieldObjectTypes obj = this.PlayField.GetCellObject(i, j);
                    if ((obj == FieldObjectTypes.FreeCell) || (obj == FieldObjectTypes.BusyCell))
                    {
                        this.PlayField.AntInstance = new Ant(i, j);
                        return;
                    }
                }
            }
        }

        /// <summary>
        /// Уведомляет о событии изменения поля.
        /// </summary>
        /// <param name="e">
        /// Аргументы сообщения. 
        /// </param>
        public void OnFieldChanged(EventArgs e)
        {
            if (this.FieldChanged != null)
            {
                this.FieldChanged(this, e);
            }
        }

        /// <summary>
        /// Уведомляет о событии изменения программы управления.
        /// </summary>
        /// <param name="e">
        /// Аргументы сообщения. 
        /// </param>
        public void OnTaskChanged(EventArgs e)
        {
            if (this.TaskChanged != null)
            {
                this.TaskChanged(this, e);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Возвращает объект менеджера.
        /// </summary>
        /// <returns>
        /// Объект, содержащий менеджера. 
        /// </returns>
        internal static Manager GetInstance()
        {
            return instance ?? (instance = new Manager());
        }

        #endregion
    }
}