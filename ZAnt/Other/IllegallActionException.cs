﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IllegallActionException.cs" company="">
//   
// </copyright>
// <summary>
//   Исключение, возникающее при невозможности выполнить действие.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ZAnt.Other
{
    using System;

    using ZAnt.ProgramEditing;

    /// <summary>
    /// Исключение, возникающее при невозможности выполнить действие.
    /// </summary>
    internal class IllegallActionException : Exception
    {
        #region Constructors and Destructors

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="IllegallActionException"/>.
        /// </summary>
        /// <param name="command">
        /// Команда, вызвавшая исключение. 
        /// </param>
        public IllegallActionException(AbstractCommand command)
            : this("Невозможно выполнить действие!", command)
        {
        }

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="IllegallActionException"/> заданным сообщением об ошибке.
        /// </summary>
        /// <param name="message">
        /// Сообщение, описывающее ошибку.
        /// </param>
        /// <param name="command">
        /// Команда, вызвавшая исключение.
        /// </param>
        public IllegallActionException(string message, AbstractCommand command)
            : base(message)
        {
            this.Command = command;
        }

        #endregion

        #region Public Properties

        /// <summary>
        ///   Получает команду, вызвавшую исключение.
        /// </summary>
        public AbstractCommand Command { get; private set; }

        #endregion
    }
}