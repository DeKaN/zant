﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Analyzer.cs" company="">
//   
// </copyright>
// <summary>
//   Класс анализатора.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ZAnt.Other
{
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.Threading;

    using ZAnt.FieldEditing;
    using ZAnt.ProgramEditing;

    /// <summary>
    /// Класс анализатора.
    /// </summary>
    internal class Analyzer
    {
        #region Constants and Fields

        /// <summary>
        /// The one by one execution commands.
        /// </summary>
        private readonly IList<AbstractCommand> oneByOneExecutionCommands = new List<AbstractCommand>();

        /// <summary>
        /// The one by one current command index.
        /// </summary>
        private int oneByOneCurrentCommandIndex;

        /// <summary>
        /// The one by one execution prepared.
        /// </summary>
        private bool oneByOneExecutionPrepared;

        #endregion

        #region Public Properties

        /// <summary>
        /// Получает ссылку на текущую команду.
        /// </summary>
        public AbstractCommand CurrentCommand { get; private set; }

        #endregion

        #region Public Methods and Operators

        /// <summary>
        /// Выполнить команду.
        /// </summary>
        /// <param name="worker">
        /// The worker. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения. 
        /// </param>
        /// <param name="command">
        /// Команда, которую надо выполнить. 
        /// </param>
        /// <exception cref="IllegalCommandTypeException">
        /// Выкидывает исключение, если встречена неизвестная команда
        /// </exception>
        /// <exception cref="IllegallActionException">
        /// Выкидывает исключение, если невозможно выполнить команду для данной позиции муравья
        /// </exception>
        public void ExecuteCommand(BackgroundWorker worker, DoWorkEventArgs e, AbstractCommand command)
        {
            if (worker.CancellationPending)
            {
                e.Cancel = true;
            }
            else
            {
                this.CurrentCommand = command;
                worker.ReportProgress(0); // TODO: заменить число (надо ли?)
                if (command.CommandType == CommandType.Cycle)
                {
                    Cycle c = command as Cycle;
                    Debug.Assert(c != null, "Ошибка при распознавании цикла");
                    for (int i = 0; i < c.RepeatCount; i++)
                    {
                        this.ExecuteCommands(worker, e, c.Commands);
                    }
                }
                else
                {
                    Command comm = command as Command;
                    Debug.Assert(comm != null, "Ошибка при распознавании команды");
                    Field field = Manager.GetInstance().PlayField;
                    Ant ant = field.AntInstance;
                    ant.CurrentDirection = comm.Direction;
                    int addX = 0, addY = 0;
                    switch (comm.Direction)
                    {
                        case Direction.Up:
                            addY = -1;
                            break;
                        case Direction.Right:
                            addX = 1;
                            break;
                        case Direction.Down:
                            addY = 1;
                            break;
                        case Direction.Left:
                            addX = -1;
                            break;
                    }

                    int currX = ant.X + addX, currY = ant.Y + addY;
                    FieldObjectTypes obj = field.GetCellObject(currX, currY);
                    switch (comm.CommandType)
                    {
                        case CommandType.Move:
                            if ((obj != FieldObjectTypes.FreeCell) && (obj != FieldObjectTypes.BusyCell))
                            {
                                throw new IllegallActionException("Невозможно шагнуть в данную клетку.", comm);
                            }

                            break;
                        case CommandType.Push:
                            if (obj != FieldObjectTypes.Stone)
                            {
                                throw new IllegallActionException("Сдвигать можно только камни!", comm);
                            }

                            obj = field.GetCellObject(currX + addX, currY + addY);
                            switch (obj)
                            {
                                case FieldObjectTypes.FreeCell:
                                    field.MoveObject(currX, currY, currX + addX, currY + addY);
                                    break;
                                case FieldObjectTypes.Hole:
                                    field.DelObject(currX, currY);
                                    field.DelObject(currX + addX, currY + addY);
                                    break;
                                default:
                                    throw new IllegallActionException("Невозможно сдвинуть данный камень.", comm);
                            }

                            break;
                        case CommandType.Jump:
                            if (obj != FieldObjectTypes.Hole)
                            {
                                throw new IllegallActionException("Прыгать можно только через лунки!", comm);
                            }

                            obj = field.GetCellObject(currX + addX, currY + addY);
                            if ((obj != FieldObjectTypes.FreeCell) && (obj != FieldObjectTypes.BusyCell))
                            {
                                throw new IllegallActionException("Нельзя приземлиться на камень/лунку!", comm);
                            }

                            currX += addX;
                            currY += addY;
                            break;
                        default:
                            throw new IllegalCommandTypeException("Ошибка, встречен неизвестный тип команды!", comm);
                    }

                    ant.X = currX;
                    ant.Y = currY;
                }
            }
        }

        /// <summary>
        /// The execute one command.
        /// </summary>
        /// <param name="worker">
        /// The worker.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        /// <returns>
        /// The execute one command.
        /// </returns>
        public bool ExecuteOneCommand(BackgroundWorker worker, DoWorkEventArgs e)
        {
            this.PrepareOneByOneExecution();

            if (this.oneByOneExecutionCommands.Count == 0)
            {
                return this.oneByOneExecutionPrepared = false;
            }

            this.ExecuteCommand(worker, e, this.oneByOneExecutionCommands[this.oneByOneCurrentCommandIndex++]);

            if (this.oneByOneCurrentCommandIndex >= this.oneByOneExecutionCommands.Count)
            {
                this.oneByOneExecutionPrepared = false;
            }

            return this.oneByOneExecutionPrepared;
        }

        /// <summary>
        /// Выполнить всю программу управления.
        /// </summary>
        /// <param name="worker">
        /// The worker. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения. 
        /// </param>
        public void ExecuteProgram(BackgroundWorker worker, DoWorkEventArgs e)
        {
            this.ExecuteCommands(worker, e, Manager.GetInstance().TaskInstance.GetCommands());
        }

        /// <summary>
        /// The reset execute one command.
        /// </summary>
        public void ResetExecuteOneCommand()
        {
            this.oneByOneCurrentCommandIndex = 0;
            this.oneByOneExecutionPrepared = false;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The add command to list.
        /// </summary>
        /// <param name="command">
        /// The command.
        /// </param>
        private void AddCommandToList(AbstractCommand command)
        {
            if (command.CommandType == CommandType.Cycle)
            {
                Cycle cycle = command as Cycle;
                if (cycle == null)
                {
                    return;
                }

                for (int i = 0; i < cycle.RepeatCount; i++)
                {
                    this.AddCommandsToList(cycle.Commands);
                }
            }
            else
            {
                this.oneByOneExecutionCommands.Add(command);
            }
        }

        /// <summary>
        /// The add commands to list.
        /// </summary>
        /// <param name="commands">
        /// The commands.
        /// </param>
        private void AddCommandsToList(IList<AbstractCommand> commands)
        {
            foreach (AbstractCommand command in commands)
            {
                this.AddCommandToList(command);
            }
        }

        /// <summary>
        /// Выполнить список команд.
        /// </summary>
        /// <param name="worker">
        /// The worker. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения. 
        /// </param>
        /// <param name="commands">
        /// Список команд для выполнения. 
        /// </param>
        private void ExecuteCommands(BackgroundWorker worker, DoWorkEventArgs e, IList<AbstractCommand> commands)
        {
            foreach (AbstractCommand abstractCommand in commands)
            {
                if (worker.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }

                this.ExecuteCommand(worker, e, abstractCommand);
                Thread.Sleep(Manager.GetInstance().Latency);
            }
        }

        /// <summary>
        /// The prepare one by one execution.
        /// </summary>
        private void PrepareOneByOneExecution()
        {
            if (this.oneByOneExecutionPrepared)
            {
                return;
            }

            this.oneByOneExecutionCommands.Clear();
            this.AddCommandsToList(Manager.GetInstance().TaskInstance.GetCommands());

            this.oneByOneCurrentCommandIndex = 0;
            this.oneByOneExecutionPrepared = true;
        }

        #endregion
    }
}