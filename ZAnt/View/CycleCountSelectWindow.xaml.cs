﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CycleCountSelectWindow.xaml.cs" company="">
//   
// </copyright>
// <summary>
//   Логика взаимодействия для CycleCountSelectWindow.xaml
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ZAnt.View
{
    using System.Windows;
    using System.Windows.Input;

    /// <summary>
    /// Логика взаимодействия для CycleCountSelectWindow.xaml
    /// </summary>
    public partial class CycleCountSelectWindow : Window
    {
        #region Constructors and Destructors

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="CycleCountSelectWindow"/> .
        /// </summary>
        public CycleCountSelectWindow()
        {
            this.InitializeComponent();
            for (int i = 1; i <= 255; i++)
            {
                this.comboBox1.Items.Add(i);
            }

            this.comboBox1.SelectedIndex = 0;
            this.CountOfLoop = (int)this.comboBox1.SelectedItem;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Получает количество итераций для цикла.
        /// </summary>
        internal int CountOfLoop { get; private set; }

        #endregion

        #region Methods

        /// <summary>
        /// Обрабатывает нажатие кнопки "Отмена".
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения. 
        /// </param>
        private void BtnCancelClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        /// <summary>
        /// Обрабатывает нажатие кнопки "Ок".
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения. 
        /// </param>
        private void BtnOkClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.CountOfLoop = (int)this.comboBox1.SelectedItem;
            this.Close();
        }

        /// <summary>
        /// Обрабатывает перетаскивание окна за прямоугольник.
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения. 
        /// </param>
        private void TitleMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        #endregion
    }
}