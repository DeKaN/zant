﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MainWindow.xaml.cs" company="">
//   
// </copyright>
// <summary>
//   Логика взаимодействия для MainWindow.xaml
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ZAnt.View
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.IO;
    using System.Text;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    using Microsoft.Win32;

    using ZAnt.FieldEditing;
    using ZAnt.Other;
    using ZAnt.ProgramEditing;

    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Fields

        /// <summary>
        /// Ссылка для хранения объекта, отвечающего за отрисовку поля.
        /// </summary>
        private readonly FieldControl fieldPart = new FieldControl();

        /// <summary>
        /// Ссылка для хранения объекта менеджера.
        /// </summary>
        private readonly Manager manager = Manager.GetInstance();

        /// <summary>
        /// Диалог открытия файла.
        /// </summary>
        private readonly OpenFileDialog openDialog = new OpenFileDialog();

        /// <summary>
        /// Диалог сохранения файла.
        /// </summary>
        private readonly SaveFileDialog saveDialog = new SaveFileDialog();

        /// <summary>
        /// The task analyzer.
        /// </summary>
        private readonly Analyzer taskAnalyzer = new Analyzer();

        /// <summary>
        /// Фоновый поток.
        /// </summary>
        private readonly BackgroundWorker worker = new BackgroundWorker();

        /// <summary>
        /// The backup field.
        /// </summary>
        private Field backupField;

        /// <summary>
        /// Выделенная команда.
        /// </summary>
        private TreeViewItem coloredItem;

        /// <summary>
        /// Соответствие команды и элемента дерева.
        /// </summary>
        private Dictionary<AbstractCommand, TreeViewItem> commandMap;

        /// <summary>
        /// Перетаскиваемая комманда (для реализации Drag'n'Drop)
        /// </summary>
        private TreeViewItem draggedItem;

        /// <summary>
        /// Точка нажатия ЛКП (для реализации Drag'n'Drop)
        /// </summary>
        private Point lastMouseDown;

        /// <summary>
        /// The program changed.
        /// </summary>
        private bool programChanged = true;

        /// <summary>
        /// Целевое место назначения (для реализации Drag'n'Drop)
        /// </summary>
        private object target;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="MainWindow"/> .
        /// </summary>
        public MainWindow()
        {
            this.InitializeComponent();

            this.worker.WorkerReportsProgress = this.worker.WorkerSupportsCancellation = true;
            this.worker.DoWork += this.RunTask;
            this.worker.ProgressChanged += this.TaskProgressChanged;
            this.worker.RunWorkerCompleted += this.TaskCompleted;
            this.manager.TaskInstance = new Task();
            this.manager.TaskChanged += this.MainWindowTaskChanged;
            this.manager.TaskInstance.AddCommand(
                new Cycle(
                    1, 
                    new List<AbstractCommand>
                        {
                            new Command { CommandType = CommandType.Push, Direction = Direction.Down }, 
                            new Command { CommandType = CommandType.Move, Direction = Direction.Down }, 
                            new Command { CommandType = CommandType.Jump, Direction = Direction.Left }, 
                            new Command { CommandType = CommandType.Push, Direction = Direction.Up }, 
                            new Command { CommandType = CommandType.Move, Direction = Direction.Up }
                        }));
            this.manager.TaskInstance.AddCommand(
                new Command { CommandType = CommandType.Jump, Direction = Direction.Right });
            this.manager.TaskInstance.AddCommand(
                new Command { CommandType = CommandType.Jump, Direction = Direction.Right });
            this.manager.TaskInstance.AddCommand(
                new Command { CommandType = CommandType.Move, Direction = Direction.Right });
            this.manager.TaskInstance.AddCommand(
                new Cycle(
                    1, 
                    new List<AbstractCommand>
                        {
                            new Command { CommandType = CommandType.Push, Direction = Direction.Down }, 
                            new Command { CommandType = CommandType.Move, Direction = Direction.Down }, 
                            new Command { CommandType = CommandType.Jump, Direction = Direction.Left }, 
                            new Command { CommandType = CommandType.Push, Direction = Direction.Up }, 
                            new Command { CommandType = CommandType.Move, Direction = Direction.Up }
                        }));
            this.manager.CreateField(8, 8, false);
            this.mainGrid.Children.Add(this.fieldPart);
            Grid.SetColumn(this.fieldPart, 3);
            Grid.SetRow(this.fieldPart, 4);
            this.fieldPart.VerticalAlignment = VerticalAlignment.Top;
            this.fieldPart.HorizontalAlignment = HorizontalAlignment.Center;
            this.fieldPart.Margin = new Thickness(0, 10, 0, 0);
            this.programItem.Tag = new object[] { this.manager.TaskInstance, null };

            this.moveLeft.Tag = new Command { CommandType = CommandType.Move, Direction = Direction.Left };
            this.moveUp.Tag = new Command { CommandType = CommandType.Move, Direction = Direction.Up };
            this.moveRight.Tag = new Command { CommandType = CommandType.Move, Direction = Direction.Right };
            this.moveDown.Tag = new Command { CommandType = CommandType.Move, Direction = Direction.Down };

            this.jumpLeft.Tag = new Command { CommandType = CommandType.Jump, Direction = Direction.Left };
            this.jumpUp.Tag = new Command { CommandType = CommandType.Jump, Direction = Direction.Up };
            this.jumpRight.Tag = new Command { CommandType = CommandType.Jump, Direction = Direction.Right };
            this.jumpDown.Tag = new Command { CommandType = CommandType.Jump, Direction = Direction.Down };

            this.pushLeft.Tag = new Command { CommandType = CommandType.Push, Direction = Direction.Left };
            this.pushUp.Tag = new Command { CommandType = CommandType.Push, Direction = Direction.Up };
            this.pushRight.Tag = new Command { CommandType = CommandType.Push, Direction = Direction.Right };
            this.pushDown.Tag = new Command { CommandType = CommandType.Push, Direction = Direction.Down };

            this.cycle.Tag = new Cycle(0, new List<AbstractCommand>());
        }

        #endregion

        #region Methods

        /// <summary>
        /// The find visual parent.
        /// </summary>
        /// <param name="child">
        /// The child. 
        /// </param>
        /// <typeparam name="TObject">
        /// </typeparam>
        /// <returns>
        /// </returns>
        private static TObject FindVisualParent<TObject>(UIElement child) where TObject : UIElement
        {
            if (child == null)
            {
                return null;
            }

            UIElement parent = VisualTreeHelper.GetParent(child) as UIElement;

            while (parent != null)
            {
                TObject found = parent as TObject;
                if (found != null)
                {
                    return found;
                }

                parent = VisualTreeHelper.GetParent(parent) as UIElement;
            }

            return null;
        }

        /// <summary>
        /// The copy item.
        /// </summary>
        /// <param name="sourceItem">
        /// The _source item. 
        /// </param>
        /// <param name="targetItem">
        /// The _target item. 
        /// </param>
        private void CopyItem(TreeViewItem sourceItem, TreeViewItem targetItem)
        {
            object[] obj = (object[])targetItem.Tag;
            AbstractCommand sourceCommand = sourceItem.Tag as AbstractCommand, addCommand;
            CommandType cmdType = sourceCommand.CommandType;

            if (cmdType == CommandType.Cycle)
            {
                CycleCountSelectWindow cycleCountSelectWindow = new CycleCountSelectWindow();
                if (!cycleCountSelectWindow.ShowDialog().Value)
                {
                    return;
                }

                addCommand = new Cycle(cycleCountSelectWindow.CountOfLoop, new List<AbstractCommand>());
            }
            else
            {
                addCommand = new Command { CommandType = cmdType, Direction = ((Command)sourceCommand).Direction };
            }

            Task task = obj[0] as Task;
            if (task != null)
            {
                task.InsertCommand(0, addCommand);
                this.manager.OnTaskChanged(new EventArgs());
                return;
            }

            Cycle c = obj[0] as Cycle;
            if (c != null)
            {
                // если целевой пункт - цикл
                PositionSelectWindow positionSelectWindow = new PositionSelectWindow();
                if (!positionSelectWindow.ShowDialog().Value)
                {
                    return;
                }

                if (positionSelectWindow.Position == CommandPosition.In)
                {
                    if (cmdType == CommandType.Cycle)
                    {
                        if (int.Parse(obj[2].ToString()) >= Config.GetInstance().MaxCycleNesting)
                        {
                            MessageBox.Show(
                                "Достигнута максимально возможная вложенность циклов", 
                                "Ошибка!", 
                                MessageBoxButton.OK, 
                                MessageBoxImage.Error);
                            return;
                        }
                    }

                    c.Commands.Insert(0, addCommand);
                    this.manager.OnTaskChanged(new EventArgs());
                    return;
                }
            }

            // если целевой пункт - команда (вставить новую команду за ней)
            object[] parentTag = (object[])FindVisualParent<TreeViewItem>(targetItem).Tag;

            Task t = parentTag[0] as Task;
            if (t != null)
            {
                t.InsertCommand((int)obj[1] + 1, addCommand);
            }
            else
            {
                Cycle parCycle = parentTag[0] as Cycle;
                if (parCycle != null)
                {
                    parCycle.Commands.Insert((int)obj[1] + 1, addCommand);
                }
            }

            this.manager.OnTaskChanged(new EventArgs());
        }

        /// <summary>
        /// The delete item.
        /// </summary>
        /// <param name="sourceItem">
        /// The _source item. 
        /// </param>
        private void DeleteItem(TreeViewItem sourceItem)
        {
            try
            {
                if (((object[])sourceItem.Tag)[0] is Task)
                {
                    if (MessageBox.Show(
                        "Вы действительно хотите удалить программу целиком?", 
                        string.Empty, 
                        MessageBoxButton.YesNo, 
                        MessageBoxImage.Question) == MessageBoxResult.Yes)
                    {
                        this.manager.TaskInstance = new Task();
                    }

                    this.programItem.Tag = new object[] { this.manager.TaskInstance, null };
                }
                else
                {
                    object[] parentTag = (object[])FindVisualParent<TreeViewItem>(sourceItem).Tag;

                    int index = (int)((object[])sourceItem.Tag)[1];

                    if (parentTag[0] is Task)
                    {
                        ((Task)parentTag[0]).DeleteCommand(index);
                    }

                    if (parentTag[0] is Cycle)
                    {
                        ((Cycle)parentTag[0]).Commands.RemoveAt(index);
                    }
                }

                this.manager.OnTaskChanged(new EventArgs());
            }
            catch (Exception exc)
            {
                Debug.Assert(false, exc.Message);
            }
        }

        /// <summary>
        /// The draw commands.
        /// </summary>
        /// <param name="progItem">
        /// The program item. 
        /// </param>
        /// <param name="commands">
        /// The commands. 
        /// </param>
        /// <param name="nesting">
        /// The nesting. 
        /// </param>
        private void DrawCommands(TreeViewItem progItem, IList<AbstractCommand> commands, int nesting)
        {
            for (int i = 0; i < commands.Count; i++)
            {
                StringBuilder header = new StringBuilder();
                TreeViewItem newTreeViewItem = new TreeViewItem { IsExpanded = true };
                if (commands[i].CommandType != CommandType.Cycle)
                {
                    Command newCommand = commands[i] as Command;
                    if (newCommand == null)
                    {
                        continue;
                    }

                    switch (newCommand.CommandType)
                    {
                        case CommandType.Jump:
                            header.Append("ПРЫГНУТЬ ");
                            break;
                        case CommandType.Push:
                            header.Append("СДВИНУТЬ ");
                            break;
                        case CommandType.Move:
                            header.Append("ШАГНУТЬ ");
                            break;
                    }

                    switch (newCommand.Direction)
                    {
                        case Direction.Up:
                            header.Append("↑");
                            break;
                        case Direction.Down:
                            header.Append("↓");
                            break;
                        case Direction.Left:
                            header.Append("←");
                            break;
                        case Direction.Right:
                            header.Append("→");
                            break;
                    }

                    newTreeViewItem.Header = header.ToString();
                    newTreeViewItem.Tag = new object[] { newCommand, i };
                    this.commandMap.Add(newCommand, newTreeViewItem);
                    progItem.Items.Add(newTreeViewItem);
                }
                else
                {
                    Cycle newCycle = commands[i] as Cycle;
                    if (newCycle == null)
                    {
                        continue;
                    }

                    header.Append("ПОВТОРИТЬ ").Append(newCycle.RepeatCount).Append(" РАЗ");
                    int number = newCycle.RepeatCount;
                    if (number > 99)
                    {
                        number %= 100;
                    }

                    int last = number % 10;
                    if (((last > 1) && (last < 5)) && (number < 11 || number > 14))
                    {
                        header.Append("А");
                    }

                    newTreeViewItem.Header = header.ToString();
                    newTreeViewItem.Tag = new object[] { newCycle, i, nesting };
                    progItem.Items.Add(newTreeViewItem);
                    this.commandMap.Add(newCycle, newTreeViewItem);
                    this.DrawCommands(newTreeViewItem, newCycle.Commands, nesting + 1);
                }
            }
        }

        /// <summary>
        /// Обрабатывает нажатие кнопки "Выход".
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения. 
        /// </param>
        private void ExitButtonClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Обрабатывает нажатие кнопки "Во весь экран/Нормальный режим".
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения. 
        /// </param>
        private void FullScreenClick(object sender, RoutedEventArgs e)
        {
            if (this.WindowState == WindowState.Normal)
            {
                this.WindowState = WindowState.Maximized;
                this.fullscreenIcon.Source = new BitmapImage(new Uri("pack://application:,,,/images/nofullscreen.ico"));
                this.fullScreenButton.ToolTip = "Выйти из полноэкранного режима";
            }
            else
            {
                this.WindowState = WindowState.Normal;
                this.fullscreenIcon.Source = new BitmapImage(new Uri("pack://application:,,,/images/fullscreen.ico"));
                this.fullScreenButton.ToolTip = "Во весь экран";
            }
        }

        /// <summary>
        /// The get nearest tree view item.
        /// </summary>
        /// <param name="element">
        /// The element. 
        /// </param>
        /// <returns>
        /// </returns>
        private TreeViewItem GetNearestTreeViewItem(UIElement element)
        {
            TreeViewItem container = element as TreeViewItem;

            while ((container == null) && (element != null))
            {
                element = VisualTreeHelper.GetParent(element) as UIElement;
                container = element as TreeViewItem;
            }

            return container;
        }

        /// <summary>
        /// The window 1_ task changed.
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения. 
        /// </param>
        private void MainWindowTaskChanged(object sender, EventArgs e)
        {
            this.programItem.Items.Clear();
            this.programItem.IsExpanded = true;
            this.commandMap = new Dictionary<AbstractCommand, TreeViewItem>();
            this.DrawCommands(this.programItem, this.manager.TaskInstance.GetCommands(), 1);
            this.programChanged = true;
        }

        /// <summary>
        /// Обрабатывает щелчок по пункту меню "Поле-&gt;Загрузить...".
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения. 
        /// </param>
        private void MenuItemFieldLoadClick(object sender, RoutedEventArgs e)
        {
            this.openDialog.Title = "Загрузить игровое поле";
            this.openDialog.DefaultExt = ".pfz";
            this.openDialog.Filter = "Игровое поле (*.pfz)|*.pfz";
            this.openDialog.FileName = string.Empty;
            if (this.openDialog.ShowDialog().Value)
            {
                using (
                    FileStream fs = new FileStream(
                        this.openDialog.FileName, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    this.manager.PlayField = Field.Load(fs);
                }
            }
        }

        /// <summary>
        /// Обрабатывает щелчок по пункту меню "Поле-&gt;Создать...".
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения. 
        /// </param>
        private void MenuItemFieldNewClick(object sender, RoutedEventArgs e)
        {
            (new CreateFieldWindow()).ShowDialog();
        }

        /// <summary>
        /// Обрабатывает щелчок по кнопке "Перезагрузить".
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения. 
        /// </param>
        private void MenuItemFieldReloadClick(object sender, RoutedEventArgs e)
        {
            this.manager.PlayField = new Field(this.backupField);
            this.backupField = null;
            this.reloadFieldButton.IsEnabled = false;
        }

        /// <summary>
        /// Обрабатывает щелчок по пункту меню "Программа-&gt;Сохранить...".
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения. 
        /// </param>
        private void MenuItemFieldSaveClick(object sender, RoutedEventArgs e)
        {
            this.saveDialog.Title = "Сохранить игровое поле";
            this.saveDialog.DefaultExt = ".pfz";
            this.saveDialog.Filter = "Игровое поле (*.pfz)|*.pfz";
            this.saveDialog.FileName = string.Empty;
            if (this.saveDialog.ShowDialog().Value)
            {
                using (
                    FileStream fs = new FileStream(
                        this.saveDialog.FileName, FileMode.Create, FileAccess.Write, FileShare.Read))
                {
                    this.manager.PlayField.Save(fs);
                }
            }
        }

        /// <summary>
        /// Обрабатывает щелчок по пункту меню "О программе".
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения. 
        /// </param>
        private void MenuItemHelpAboutClick(object sender, RoutedEventArgs e)
        {
            (new AboutBox()).ShowDialog();
        }

        /// <summary>
        /// The menu item help click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void MenuItemHelpClick(object sender, RoutedEventArgs e)
        {
            string path = Path.Combine(Path.GetTempPath(), "help.chm");
            File.WriteAllBytes(path, Properties.Resources.help);
            Process.Start(path);
        }

        /// <summary>
        /// Обрабатывает щелчок по пункту меню "Выполнение-&gt;Сделать шаг".
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения. 
        /// </param>
        private void MenuItemMakeStepClick(object sender, RoutedEventArgs e)
        {
            if (this.worker.IsBusy)
            {
                this.worker.CancelAsync();
            }
            else
            {
                this.PrepareRunningProgram();
                this.worker.RunWorkerAsync(this.programChanged);
            }
        }

        /// <summary>
        /// Обрабатывает щелчок по пункту меню "Программа-&gt;Загрузить...".
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения. 
        /// </param>
        private void MenuItemProgramLoadClick(object sender, RoutedEventArgs e)
        {
            this.openDialog.Title = "Загрузить программу управления";
            this.openDialog.DefaultExt = ".mpz";
            this.openDialog.Filter = "Программа управления (*.mpz)|*.mpz";
            this.openDialog.FileName = string.Empty;
            if (this.openDialog.ShowDialog().Value)
            {
                using (
                    FileStream fs = new FileStream(
                        this.openDialog.FileName, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    this.manager.TaskInstance = Task.Load(fs);
                }
            }
        }

        /// <summary>
        /// Обрабатывает щелчок по пункту меню "Программа-&gt;Создать...".
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения. 
        /// </param>
        private void MenuItemProgramNewClick(object sender, RoutedEventArgs e)
        {
            this.manager.TaskInstance = new Task();
            this.programItem.Tag = new object[] { this.manager.TaskInstance, null };
        }

        /// <summary>
        /// Обрабатывает щелчок по пункту меню "Программа-&gt;Сохранить...".
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения. 
        /// </param>
        private void MenuItemProgramSaveClick(object sender, RoutedEventArgs e)
        {
            this.saveDialog.Title = "Сохранить программу управления";
            this.saveDialog.DefaultExt = ".mpz";
            this.saveDialog.Filter = "Программа управления (*.mpz)|*.mpz";
            this.saveDialog.FileName = string.Empty;
            if (this.saveDialog.ShowDialog().Value)
            {
                using (
                    FileStream fs = new FileStream(
                        this.saveDialog.FileName, FileMode.Create, FileAccess.Write, FileShare.Read))
                {
                    this.manager.TaskInstance.Save(fs);
                }
            }
        }

        /// <summary>
        /// Обрабатывает щелчок по пункту меню "Выполнение-&gt;Запустить".
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения. 
        /// </param>
        private void MenuItemRunClick(object sender, RoutedEventArgs e)
        {
            if (this.worker.IsBusy)
            {
                this.worker.CancelAsync();
            }
            else
            {
                this.PrepareRunningProgram();
                this.runProgramIcon.Source = new BitmapImage(new Uri("pack://application:,,,/images/stop.ico"));
                this.runProgramButton.ToolTip = "Остановить выполнение";
                this.speedSlider.Visibility = this.labelSpeed.Visibility = Visibility.Visible;
                this.worker.RunWorkerAsync();
            }
        }

        /// <summary>
        /// Подготовка к запуску программы управления муравьем.
        /// </summary>
        private void PrepareRunningProgram()
        {
            if (this.backupField == null)
            {
                this.backupField = new Field(this.manager.PlayField);
            }

            this.reloadFieldButton.IsEnabled = true;
            this.fieldPart.EditMode = false;
            this.manager.IsTaskRunning = true;
        }

        /// <summary>
        /// The recycle image_ drop.
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения. 
        /// </param>
        private void RecycleImageDrop(object sender, DragEventArgs e)
        {
            try
            {
                e.Effects = DragDropEffects.None;

                if (this.draggedItem != null)
                {
                    this.target = e.OriginalSource;
                    e.Effects = DragDropEffects.Move;
                }
            }
            catch (Exception exc)
            {
                Debug.Assert(false, exc.Message);
            }
        }

        /// <summary>
        /// Запускает выполнение программы управления муравья.
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения. 
        /// </param>
        private void RunTask(object sender, DoWorkEventArgs e)
        {
            if (e.Argument == null)
            {
                this.taskAnalyzer.ExecuteProgram(this.worker, e);
            }
            else
            {
                if (this.programChanged)
                {
                    this.programChanged = false;
                    this.taskAnalyzer.ResetExecuteOneCommand();
                }

                bool flag = this.taskAnalyzer.ExecuteOneCommand(this.worker, e);
            }
        }

        /// <summary>
        /// Происходит при перемещении ползунка слайдера.
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения. 
        /// </param>
        private void SliderSpeedValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            this.manager.Latency = (int)this.speedSlider.Value;
        }

        /// <summary>
        /// Обрабатывает завершение выполнения программы управления муравья.
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения. 
        /// </param>
        private void TaskCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (e.Error != null)
            {
                MessageBox.Show(e.Error.Message, "Ошибка!", MessageBoxButton.OK, MessageBoxImage.Stop);
            }

            this.speedSlider.Visibility = this.labelSpeed.Visibility = Visibility.Hidden;
            this.runProgramIcon.Source = new BitmapImage(new Uri("pack://application:,,,/images/run.ico"));
            this.runProgramButton.ToolTip = "Запустить выполнение программы";
            this.fieldPart.EditMode = true;
            this.manager.IsTaskRunning = false;
        }

        /// <summary>
        /// Срабатывает при изменении прогресса.
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения. 
        /// </param>
        private void TaskProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (this.coloredItem != null)
            {
                this.coloredItem.Background = Brushes.White;
            }

            this.coloredItem = null;
            try
            {
                this.coloredItem = this.commandMap[this.taskAnalyzer.CurrentCommand];
            }
            catch
            {
            }

            if (this.coloredItem != null)
            {
                this.coloredItem.Background = Brushes.Yellow;
            }
        }

        /// <summary>
        /// Обрабатывает перетаскивание окна за прямоугольник.
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения. 
        /// </param>
        private void TitleMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        /// <summary>
        /// The tree view commands_ drag over.
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения. 
        /// </param>
        private void TreeViewCommandsDragOver(object sender, DragEventArgs e)
        {
            try
            {
                if (this.draggedItem.Parent.Equals(this.treeViewCommands))
                {
                    Point currentPosition = e.GetPosition(this.treeViewCommands);

                    if ((Math.Abs(currentPosition.X - this.lastMouseDown.X) > 10.0)
                        || (Math.Abs(currentPosition.Y - this.lastMouseDown.Y) > 10.0))
                    {
                        e.Effects = DragDropEffects.Move;
                    }
                }
            }
            catch (Exception exc)
            {
                Debug.Assert(false, exc.Message);
            }
        }

        /// <summary>
        /// The tree view commands_ mouse down.
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения. 
        /// </param>
        private void TreeViewCommandsMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                this.lastMouseDown = e.GetPosition(this.treeViewCommands);
            }
        }

        /// <summary>
        /// The tree view commands_ mouse move.
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения. 
        /// </param>
        private void TreeViewCommandsMouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                if (e.LeftButton == MouseButtonState.Pressed)
                {
                    Point currentPosition = e.GetPosition(this.treeViewCommands);

                    if ((Math.Abs(currentPosition.X - this.lastMouseDown.X) > 10.0)
                        || (Math.Abs(currentPosition.Y - this.lastMouseDown.Y) > 10.0))
                    {
                        this.draggedItem = (TreeViewItem)this.treeViewCommands.SelectedItem;
                        if (this.draggedItem != null)
                        {
                            DragDropEffects finalDropEffect = DragDrop.DoDragDrop(
                                this.treeViewCommands, 
                                ((TreeViewItem)this.treeViewCommands.SelectedItem).Tag, 
                                DragDropEffects.Copy);

                            if (((finalDropEffect == DragDropEffects.Move) || (finalDropEffect == DragDropEffects.Copy))
                                && (this.target != null))
                            {
                                this.CopyItem(this.draggedItem, (TreeViewItem)this.target);
                            }
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                Debug.Assert(false, exc.Message);
            }
            finally
            {
                this.target = this.draggedItem = null;
            }
        }

        /// <summary>
        /// The tree view program_ drag over.
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения. 
        /// </param>
        private void TreeViewProgramDragOver(object sender, DragEventArgs e)
        {
            try
            {
                if (this.draggedItem.Parent.Equals(this.treeViewProgram))
                {
                    Point currentPosition = e.GetPosition(this.treeViewProgram);

                    if ((Math.Abs(currentPosition.X - this.lastMouseDown.X) > 10.0)
                        || (Math.Abs(currentPosition.Y - this.lastMouseDown.Y) > 10.0))
                    {
                        e.Effects = DragDropEffects.Move;
                    }
                }
            }
            catch (Exception exc)
            {
                Debug.Assert(false, exc.Message);
            }
        }

        /// <summary>
        /// The tree view program_ drop.
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения. 
        /// </param>
        private void TreeViewProgramDrop(object sender, DragEventArgs e)
        {
            try
            {
                e.Effects = DragDropEffects.None;

                TreeViewItem targetItem = this.GetNearestTreeViewItem(e.OriginalSource as UIElement);

                if (targetItem != null && this.draggedItem != null)
                {
                    this.target = targetItem;
                    e.Effects = DragDropEffects.Move;
                }
            }
            catch (Exception exc)
            {
                Debug.Assert(false, exc.Message);
            }
        }

        /// <summary>
        /// The tree view program_ mouse down.
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения. 
        /// </param>
        private void TreeViewProgramMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                this.lastMouseDown = e.GetPosition(this.treeViewProgram);
            }
        }

        /// <summary>
        /// The tree view program_ mouse move.
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения. 
        /// </param>
        private void TreeViewProgramMouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                if (e.LeftButton == MouseButtonState.Pressed)
                {
                    Point currentPosition = e.GetPosition(this.treeViewProgram);

                    if ((Math.Abs(currentPosition.X - this.lastMouseDown.X) > 10.0)
                        || (Math.Abs(currentPosition.Y - this.lastMouseDown.Y) > 10.0))
                    {
                        this.draggedItem = (TreeViewItem)this.treeViewProgram.SelectedItem;
                        if (this.draggedItem != null)
                        {
                            DragDropEffects finalDropEffect = DragDrop.DoDragDrop(
                                this.treeViewProgram, 
                                ((TreeViewItem)this.treeViewProgram.SelectedItem).Tag, 
                                DragDropEffects.Move);

                            if (((finalDropEffect == DragDropEffects.Move) || (finalDropEffect == DragDropEffects.Copy))
                                && this.target != null && this.target is Image)
                            {
                                this.DeleteItem(this.draggedItem);
                            }
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                Debug.Assert(false, exc.Message);
            }
            finally
            {
                this.target = this.draggedItem = null;
            }
        }

        #endregion
    }
}