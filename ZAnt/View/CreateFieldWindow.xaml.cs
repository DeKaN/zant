﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CreateFieldWindow.xaml.cs" company="">
//   
// </copyright>
// <summary>
//   Логика взаимодействия для CreateFieldWindow.xaml
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ZAnt.View
{
    using System.Windows;
    using System.Windows.Input;

    using ZAnt.Other;

    /// <summary>
    /// Логика взаимодействия для CreateFieldWindow.xaml
    /// </summary>
    public partial class CreateFieldWindow : Window
    {
        #region Constants and Fields

        /// <summary>
        ///   Заполнять ли автоматически поле.
        /// </summary>
        private bool auto;

        /// <summary>
        ///   Высота нового поля.
        /// </summary>
        private int h = 5;

        /// <summary>
        ///   Ширина нового поля.
        /// </summary>
        private int w = 5;

        #endregion

        #region Constructors and Destructors

        /// <summary>
        ///   Инициализирует новый экземпляр класса <see cref="CreateFieldWindow" />.
        /// </summary>
        public CreateFieldWindow()
        {
            this.InitializeComponent();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Обрабатывает нажатие кнопки "Отмена".
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения.
        /// </param>
        private void BtnCancelClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        /// <summary>
        /// Обрабатывает нажатие кнопки "Ок".
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения.
        /// </param>
        private void BtnOkClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            Manager.GetInstance().CreateField(this.w, this.h, this.auto);
            this.Close();
        }

        /// <summary>
        /// Обрабатывает смену состояния флажка.
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения.
        /// </param>
        private void CheckBoxAutoCheckedChanged(object sender, RoutedEventArgs e)
        {
            this.auto = this.checkBoxAuto.IsChecked.Value;
        }

        /// <summary>
        /// Обрабатывает изменение значений ползунков.
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения.
        /// </param>
        private void SlidersValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            this.w = this.sliderW != null ? (int)this.sliderW.Value : 5;
            this.h = this.sliderH != null ? (int)this.sliderH.Value : 5;
        }

        /// <summary>
        /// Обрабатывает перетаскивание окна за прямоугольник.
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения.
        /// </param>
        private void TitleMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        #endregion
    }
}