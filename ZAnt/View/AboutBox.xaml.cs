﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AboutBox.xaml.cs" company="">
//   
// </copyright>
// <summary>
//   Логика взаимодействия для AboutBox.xaml
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ZAnt.View
{
    using System.Windows;
    using System.Windows.Input;

    /// <summary>
    /// Логика взаимодействия для AboutBox.xaml
    /// </summary>
    public partial class AboutBox : Window
    {
        #region Constructors and Destructors

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="AboutBox" />.
        /// </summary>
        public AboutBox()
        {
            this.InitializeComponent();
        }

        #endregion

        #region Methods

        /// <summary>
        /// Обрабатывает нажатие кнопки "Ок".
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения.
        /// </param>
        private void BtnOkClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Обрабатывает перетаскивание окна за прямоугольник.
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения.
        /// </param>
        private void TitleMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        #endregion
    }
}