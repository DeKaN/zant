﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PositionSelectWindow.xaml.cs" company="">
//   
// </copyright>
// <summary>
//   Логика взаимодействия для PositionSelectWindow.xaml
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace ZAnt.View
{
    using System.Windows;
    using System.Windows.Input;

    /// <summary>
    /// Позиция вставляемой комманды относительно цикла.
    /// </summary>
    internal enum CommandPosition
    {
        /// <summary>
        /// После цикла.
        /// </summary>
        After, 

        /// <summary>
        /// Внутри цикла.
        /// </summary>
        In
    }

    /// <summary>
    /// Логика взаимодействия для CycleCountSelectWindow.xaml
    /// </summary>
    public partial class PositionSelectWindow : Window
    {
        #region Constructors and Destructors

        /// <summary>
        /// Инициализирует новый экземпляр класса <see cref="PositionSelectWindow"/> .
        /// </summary>
        public PositionSelectWindow()
        {
            this.InitializeComponent();
            this.Position = this.radBtnAfter.IsChecked.Value ? CommandPosition.After : CommandPosition.In;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Получает позицию вставляемой команды относительно цикла.
        /// </summary>
        internal CommandPosition Position { get; private set; }

        #endregion

        #region Methods

        /// <summary>
        /// Обрабатывает нажатие кнопки "Отмена".
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения. 
        /// </param>
        private void BtnCancelClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
            this.Close();
        }

        /// <summary>
        /// Обрабатывает нажатие кнопки "Ок".
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения. 
        /// </param>
        private void BtnOkClick(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
            this.Position = this.radBtnAfter.IsChecked.Value ? CommandPosition.After : CommandPosition.In;
            this.Close();
        }

        /// <summary>
        /// Обрабатывает перетаскивание окна за прямоугольник.
        /// </summary>
        /// <param name="sender">
        /// Отправитель сообщения. 
        /// </param>
        /// <param name="e">
        /// Аргументы сообщения. 
        /// </param>
        private void TitleMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        #endregion
    }
}